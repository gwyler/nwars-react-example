# Example NWARS2 using Next.js with TypeScript and OpenLayers

This is a sample Next.js app that integrates TypeScript and OpenLayers for building interactive maps.

## Prerequisites

Make sure you have the following software installed on your machine:

- Node.js (v14 or higher)
- Git

## Getting Started

To get started, follow these steps:

1. Clone the repository to your local machine using the following command:

```sh
git clone <repository_url>
```
2. Install the dependencies using npm
```sh
cd <repository_directory>
npm install
```
3. Start the Next.js development server:
```sh
npm run dev
```
4. Open your web browser and go to http://localhost:3000 to see the app running locally.

## Dependencies

- @fortawesome/fontawesome-free: ^6.4.0
- @fortawesome/fontawesome-svg-core: ^6.4.0
- @fortawesome/react-fontawesome: ^0.2.0
- @reduxjs/toolkit: ^1.9.3
- @types/node: 18.15.11
- @types/react: 18.0.33
- @types/react-dom: 18.0.11
- ag-grid-community: ^29.2.0
- ag-grid-react: ^29.2.0
- bootstrap: ^5.2.3
- next: 13.2.4
- next-redux-wrapper: ^8.1.0
- ol: ^7.3.0
- react: 18.2.0
- react-dom: 18.2.0
- react-pro-sidebar: ^1.0.0
- react-redux: ^8.0.5
- react-responsive-sidebar: ^0.1.16
- react-sidebar: ^3.0.2
- redux: ^4.2.1
- redux-thunk: ^2.4.2
- typescript: 5.0.3
- xml2js: ^0.4.23

## Dev Dependencies

- @types/react-sidebar: ^3.0.2
- @types/xml2js: ^0.4.11

