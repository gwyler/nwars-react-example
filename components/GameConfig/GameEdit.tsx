import React, { useEffect, useMemo, useState } from "react";
import {
  createAndFlyToAOR,
  gameData,
  getTodaysDate,
  unifiedCombatantCommands,
} from "@/utils";
import { GameData, RootState } from "@/types";
import GameDetails from "./GameDetails";
import { useSelector } from "react-redux";

interface Props {
  selectedGame: GameData | null;
  setSelectedGame: (selectedGame: GameData | null) => void;
  setIsEditMode: (isEditMode: boolean) => void;
  setShowModal: (showModal: boolean) => void;
  games: GameData[];
  setGames: (games: GameData[]) => void;
}

const GameEdit: React.FC<Props> = ({
  selectedGame,
  setSelectedGame,
  setShowModal,
  setIsEditMode,
  games,
  setGames,
}) => {
  const { validForm, step, setStep, newGame, viewer, updatedGameData } =
    useGameEditState(selectedGame, games);

  const handleCancel = () => {
    setIsEditMode(false);
  };

  const handleNextClick = () => {
    setStep((prevStep) => prevStep + 1);
  };

  const handleBackClick = () => {
    setStep((prevStep) => prevStep - 1);
  };

  const handleCreateGame = () => {
    if (viewer && newGame) {
      setGames(updatedGameData);
      setSelectedGame(newGame);
      createAndFlyToAOR(newGame, viewer);
      setShowModal(false);
      setIsEditMode(false);
    }
  };
  return (
    <>
      <div className="modal-header justify-content-center">
        <h2>{selectedGame === null ? "Create " : "Edit "}Game</h2>
      </div>
      <div className="modal-body">
        {step === 1 && (
          <GameDetails selectedGame={selectedGame} isEditMode={true} />
        )}
        {step === 2 && (
          <h1 className="text-center text-muted">
            {`Selecting AOI's will go here`}
          </h1>
        )}
        {step === 3 && (
          <h1 className="text-center text-muted">
            {`Importing CR's will go here`}
          </h1>
        )}
      </div>
      <div className="modal-footer d-inline-flex">
        {step > 1 && step < 4 && (
          <button onClick={handleBackClick} className={"btn btn-primary"}>
            <i className="fa fa-arrow-left" /> Back
          </button>
        )}
        <button onClick={handleCancel} className={"btn btn-secondary"}>
          Cancel <i className="fa fa-ban" />
        </button>
        {step < 3 && (
          <button
            onClick={handleNextClick}
            className={`btn btn-primary ${
              !validForm && step === 1 ? "disabled" : ""
            }`}
          >
            Next <i className="fa fa-arrow-right" />
          </button>
        )}
        {step === 3 && (
          <button onClick={handleCreateGame} className={"btn btn-success"}>
            {`${selectedGame ? "Save" : "Create"} Game`}{" "}
            <i className="fa fa-save" />
          </button>
        )}
      </div>
    </>
  );
};

const useGameEditState = (selectedGame: GameData | null, games: GameData[]) => {
  // redux state
  const gameConfig = useSelector((state: RootState) => state.gameConfig);
  const viewer = useSelector((state: RootState) => state.map.viewer);
  // local state
  const [step, setStep] = useState(1);
  const [newGame, setNewGame] = useState<GameData | null>(null);
  // derived state
  const { gameName, factionName, selectedCommand, startDate, duration } =
    gameConfig;
  const validForm =
    gameName &&
    gameName.length > 0 &&
    factionName &&
    factionName.length > 0 &&
    startDate &&
    !isNaN(Date.parse(startDate)) &&
    duration &&
    duration > 0 &&
    selectedCommand &&
    selectedCommand.length > 0;

  useEffect(() => {
    const maxId = games.reduce(
      (max, game) => (game.id > max ? game.id : max),
      0
    );
    setNewGame({
      id: selectedGame ? selectedGame.id : maxId + 1,
      name: gameName,
      factionName,
      startDate,
      duration,
      status: "Incomplete",
      phase: "Initial Setup",
      createdBy: selectedGame ? selectedGame.createdBy : "<your name>",
      createdDate: selectedGame ? selectedGame.createdDate : getTodaysDate(),
      updatedDate: getTodaysDate(),
      unifiedCombatantCommand:
        Object.values(unifiedCombatantCommands).find(
          (c) => c.name === selectedCommand
        ) || unifiedCombatantCommands.pacom,
      collectionRequirements: 23,
    });
  }, [
    duration,
    factionName,
    gameName,
    games,
    selectedCommand,
    selectedGame,
    startDate,
  ]);
  const updatedGameData = useMemo(() => {
    if (viewer && newGame) {
      if (selectedGame) {
        return games.map((game) => {
          if (game.id === selectedGame.id) {
            return newGame;
          } else {
            return game;
          }
        });
      } else {
        return [...games, newGame];
      }
    } else {
      return games;
    }
  }, [games, newGame, selectedGame, viewer]);
  return { validForm, step, setStep, newGame, viewer, updatedGameData };
};

export default GameEdit;
