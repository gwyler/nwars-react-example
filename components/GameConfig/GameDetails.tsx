import React, { Dispatch, useEffect, useMemo } from "react";
import Image from "next/image";
import { getTodaysDate, unifiedCombatantCommands } from "@/utils";
import { GameData, RootState } from "@/types";
import { DropdownButton, Dropdown } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  setDuration,
  setFactionName,
  setGameName,
  setSelectedCommand,
  setStartDate,
} from "./redux/gameConfigReducer";

interface Props {
  selectedGame: GameData | null;
  isEditMode: boolean;
}

interface FormControl {
  label: string;
  element: JSX.Element;
  id: string;
}

const GameDetails: React.FC<Props> = ({ selectedGame, isEditMode }) => {
  const { formControls, commandImage } = useGameDetailsState(
    selectedGame,
    isEditMode
  );
  const maxHeight = isEditMode ? "500px" : "250px";
  return (
    <div className="container" style={{ overflowY: "auto", maxHeight }}>
      <div className="row">
        <div className="col">
          {formControls.map((control, index) => (
            <div key={index} className="d-flex flex-column mb-3">
              {control.label && (
                <label htmlFor={control.id} className="fw-bold">
                  {control.label}
                </label>
              )}
              {control.element}
            </div>
          ))}
        </div>
        <div className="col">{commandImage}</div>
      </div>
    </div>
  );
};

const useGameDetailsState = (
  selectedGame: GameData | null,
  isEditMode: boolean
) => {
  const dispatch = useDispatch();
  const { gameName, factionName, selectedCommand, startDate, duration } =
    useSelector((state: RootState) => state.gameConfig);
  const commandImageSrc = useMemo(() => {
    return (
      Object.values(unifiedCombatantCommands).find(
        (c) => c.name === selectedCommand
      )?.imageURL || ""
    );
  }, [selectedCommand]);

  useEffect(() => {
    dispatch(setGameName(selectedGame?.name) || "");
    dispatch(setFactionName(selectedGame?.factionName) || "");
    dispatch(
      setSelectedCommand(selectedGame?.unifiedCombatantCommand.name) || ""
    );
    dispatch(setStartDate(selectedGame?.startDate) || "");
    dispatch(setDuration(selectedGame?.duration) || "");
  }, [dispatch, selectedGame]);
  const formControls: FormControl[] = useMemo(
    () =>
      _getFormControls(
        gameName,
        factionName,
        selectedCommand,
        startDate,
        duration,
        dispatch,
        selectedGame,
        isEditMode
      ),
    [
      dispatch,
      duration,
      factionName,
      gameName,
      isEditMode,
      selectedCommand,
      selectedGame,
      startDate,
    ]
  );
  const commandImage = commandImageSrc !== "" && (
    <Image
      src={commandImageSrc}
      alt={selectedCommand}
      width={325}
      height={225}
    />
  );
  return { formControls, commandImage };
};

const _getFormControls = (
  gameName: string,
  factionName: string,
  selectedCommand: string,
  startDate: string,
  duration: string,
  dispatch: Dispatch<any>,
  selectedGame: GameData | null,
  isEditMode: boolean
) => {
  const hasSelectedGame = selectedGame !== null;
  const createNameControl = () => {
    if (!isEditMode && hasSelectedGame) {
      return <span>{selectedGame.name}</span>;
    }
    return (
      <input
        className="form-control bg-dark text-light"
        value={gameName}
        onChange={(event) => dispatch(setGameName(event.target.value))}
        placeholder="Enter a Game Name"
      />
    );
  };
  const createFactionNameControl = () => {
    if (!isEditMode && hasSelectedGame) {
      return <span>{selectedGame.factionName}</span>;
    }
    return (
      <input
        className="form-control bg-dark text-light"
        placeholder="Enter a Faction Name"
        value={factionName}
        onChange={(event) => dispatch(setFactionName(event.target.value))}
        min={1}
      />
    );
  };
  const createUccControl = () => {
    if (!isEditMode && hasSelectedGame) {
      return <span>{selectedGame.unifiedCombatantCommand.name}</span>;
    }
    const uccItems = Object.entries(unifiedCombatantCommands).map(
      ([key, value]) => (
        <Dropdown.Item eventKey={value.name} key={key}>
          {value.name}
        </Dropdown.Item>
      )
    );
    return (
      <DropdownButton
        title={selectedCommand || "Select a Command"}
        variant="dark"
        onSelect={(eventKey) => dispatch(setSelectedCommand(eventKey))}
        className="border border-light rounded"
      >
        {uccItems}
      </DropdownButton>
    );
  };
  const createStartDateControl = () => {
    if (!isEditMode && hasSelectedGame) {
      return <span>{selectedGame.startDate}</span>;
    }
    return (
      <input
        type="date"
        className="form-control bg-dark text-light"
        value={startDate}
        onChange={(event) => dispatch(setStartDate(event.target.value))}
      />
    );
  };
  const createDurationControl = () => {
    if (!isEditMode && hasSelectedGame) {
      return <span>{selectedGame.duration}</span>;
    }
    return (
      <input
        type="number"
        value={duration}
        className="form-control bg-dark text-light"
        placeholder="Enter Duration of Game"
        onChange={(event) => dispatch(setDuration(event.target.value))}
        min={1}
      />
    );
  };
  const nameControl = createNameControl();
  const factionNameControl = createFactionNameControl();
  const uccControl = createUccControl();
  const startDateControl = createStartDateControl();
  const durationControl = createDurationControl();
  const createdByControl = (
    <span>{hasSelectedGame ? selectedGame!.createdBy : "<your name>"}</span>
  );
  const createdDateControl = (
    <span>{hasSelectedGame ? selectedGame.createdDate : getTodaysDate()}</span>
  );
  const statusControl = (
    <span>{hasSelectedGame ? selectedGame!.status : "Incomplete"}</span>
  );
  const phaseControl = (
    <span>{hasSelectedGame ? selectedGame!.phase : "Initial Setup"}</span>
  );

  return [
    { label: "Game Name", element: nameControl, id: "game-name-control" },
    {
      label: "Faction Name",
      element: factionNameControl,
      id: "faction-name-control",
    },
    { label: "UCC", element: uccControl, id: "ucc-control" },
    {
      label: "Start Date",
      element: startDateControl,
      id: "start-date-control",
    },
    {
      label: "Duration (days)",
      element: durationControl,
      id: "duration-control",
    },
    {
      label: "Created By",
      element: createdByControl,
      id: "created-by-control",
    },
    {
      label: "Created Date",
      element: createdDateControl,
      id: "created-date-control",
    },
    { label: "Status", element: statusControl, id: "status-control" },
    { label: "Phase", element: phaseControl, id: "phase-control" },
  ];
};

export default GameDetails;
