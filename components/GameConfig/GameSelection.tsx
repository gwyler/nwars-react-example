import React, { useCallback, useState } from "react";
import { AgGridReact } from "ag-grid-react";
import { useSelector } from "react-redux";
import * as Cesium from "cesium";
import { GameData, RootState } from "@/types";
import GameDetails from "./GameDetails";
import { createAndFlyToAOR } from "@/utils";

interface Props {
  selectedGame: GameData | null;
  setSelectedGame: (selectGame: GameData | null) => void;
  setShowModal: (showModal: boolean) => void;
  setIsEditMode: (isEditMode: boolean) => void;
  games: GameData[];
}

const GameSelection: React.FC<Props> = ({
  selectedGame,
  setSelectedGame,
  setShowModal,
  setIsEditMode,
  games,
}) => {
  const { columnDefs, rowData, handleDelete, handleGameSelection } =
    useGameSelectionState(selectedGame, setShowModal, setSelectedGame, games);
  const handleEdit = useCallback(() => {
    setIsEditMode(true);
  }, [setIsEditMode]);

  const handleCreate = useCallback(() => {
    setIsEditMode(true);
    setSelectedGame(null);
  }, [setIsEditMode, setSelectedGame]);
  return (
    <>
      <div className="modal-header justify-content-center">
        <h2>Select a Game to Continue</h2>
      </div>
      <div className="modal-body">
        {selectedGame === null ? (
          <div className="text-center text-muted p-5">No Game Selected</div>
        ) : (
          <GameDetails selectedGame={selectedGame} isEditMode={false} />
        )}

        <div
          className="ag-theme-alpine-dark"
          style={{ height: "100%", width: "100%" }}
        >
          <AgGridReact
            onFirstDataRendered={(params) => params.api.sizeColumnsToFit()}
            pagination
            paginationPageSize={5}
            rowSelection="single"
            domLayout="autoHeight"
            columnDefs={columnDefs}
            rowData={rowData}
            onGridReady={(params) => {
              if (selectedGame) {
                params.api.forEachNode((node) => {
                  if (node.data && node.data.id === selectedGame.id) {
                    node.setSelected(true);
                  }
                });
              }
            }}
            onSelectionChanged={(params) => {
              setSelectedGame(params.api.getSelectedRows()[0]);
            }}
          />
        </div>
      </div>
      <div className="modal-footer d-inline-flex">
        <button onClick={handleCreate} className="btn btn-success float-left">
          New <i className="fa fa-plus" />
        </button>
        <button
          onClick={handleDelete}
          className={`btn btn-danger ${
            selectedGame === null ? "disabled" : ""
          }`}
        >
          Delete <i className="fa fa-trash" />
        </button>
        <button
          onClick={handleEdit}
          className={`btn btn-secondary ${
            selectedGame === null ? "disabled" : ""
          }`}
        >
          Edit <i className="fa fa-pencil" />
        </button>
        <button
          onClick={handleGameSelection}
          className={`btn btn-primary ${
            selectedGame === null ? "disabled" : ""
          }`}
        >
          Continue <i className="fa fa-arrow-right" />
        </button>
      </div>
    </>
  );
};

const useGameSelectionState = (
  selectedGame: GameData | null,
  setShowModal: Function,
  setSelectedGame: Function,
  games: GameData[]
) => {
  // redux state
  const viewer = useSelector(
    (state: RootState) => state.map.viewer,
    (prev, next) => prev === next
  );

  // local state
  const [columnDefs] = useState([
    {
      field: "name",
      sortable: true,
      filter: true,
      resizable: true,
    },
    {
      field: "status",
      sortable: true,
      filter: true,
      resizable: true,
    },
    {
      field: "phase",
      sortable: true,
      filter: true,
      resizable: true,
    },
    {
      field: "updatedDate",
      sortable: true,
      filter: "agDateColumnFilter",
      resizable: true,
    },
  ]);

  const [rowData, setRowData] = useState<GameData[]>(games);
  const handleGameSelection = useCallback(() => {
    if (!selectedGame || !viewer) {
      return;
    }
    createAndFlyToAOR(selectedGame, viewer);
    setShowModal(false);
  }, [selectedGame, setShowModal, viewer]);
  const handleDelete = useCallback(() => {
    if (!selectedGame) {
      return;
    }
    const confirmed = window.confirm(
      "Are you sure you want to delete this game?"
    );

    if (confirmed) {
      setRowData((prevRowData) =>
        prevRowData.filter((datum) => datum.id !== selectedGame.id)
      );
      setSelectedGame(null);
    }
  }, [selectedGame, setSelectedGame]);
  return { columnDefs, rowData, handleDelete, handleGameSelection };
};

export default GameSelection;
