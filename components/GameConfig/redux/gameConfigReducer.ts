import { GameConfigState } from "@/types";
import { createSlice } from "@reduxjs/toolkit";

const initialState: GameConfigState = {
  gameName: "",
  factionName: "",
  selectedCommand: "",
  startDate: new Date(),
  duration: 0,
};

export const mapSlice = createSlice({
  name: "map",
  initialState,
  reducers: {
    setGameName: (state, action) => {
      state.gameName = action.payload;
    },
    setFactionName: (state, action) => {
      state.factionName = action.payload;
    },
    setSelectedCommand: (state, action) => {
      state.selectedCommand = action.payload;
    },
    setStartDate: (state, action) => {
      state.startDate = action.payload;
    },
    setDuration: (state, action) => {
      state.duration = action.payload;
    },
    clearNewGameFields: () => initialState,
  },
});

export const {
  setGameName,
  setFactionName,
  setSelectedCommand,
  setStartDate,
  setDuration,
  clearNewGameFields,
} = mapSlice.actions;

export default mapSlice.reducer;
