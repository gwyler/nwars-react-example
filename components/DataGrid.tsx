import React from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/styles//ag-grid.css";
import "ag-grid-community/styles//ag-theme-alpine.css";
import { DataGridProps } from "@/types";

// Define the EntityGrid component
// This component renders an AgGridReact component with specified column definitions, row data, and title
// Props:
// - columnDefs: an array of column definitions for the AgGridReact component
// - rowData: an array of row data for the AgGridReact component
// - title: a string to display as the title of the data grid
const DataGrid: React.FC<DataGridProps> = ({ columnDefs, rowData, title }) => (
  <React.Fragment>
    {/* Render the title with row count */}
    <h5>{`${title} (${rowData?.length || 0 > 0 ? rowData?.length : ""})`}</h5>
    {/* Render the AgGridReact component */}
    <div className="ag-theme-alpine-dark" style={{ height: "500px" }}>
      <AgGridReact
        columnDefs={columnDefs}
        rowData={rowData}
        onFirstDataRendered={(params) => params.columnApi.autoSizeAllColumns()}
        enableCellTextSelection
      />
    </div>
  </React.Fragment>
);

// Export the DataGrid component as the default export
export default DataGrid;
