import { SimObject } from "@/types";
import { testData } from "@/utils";
import React from "react";

// This component displays a summary table of Game Obejects and their counts
const GameTruthSummary = () => (
  <>
    <h5>Game Truth Summary</h5>
    <table className="table table-striped">
      <thead className="thead-dark">
        <tr>
          <th>Description</th>
          <th>TOTAL</th>
          <th>Unknown</th>
          <th>Friendly</th>
          <th>Opposing</th>
          <th>Neutral</th>
          <th>Inactive</th>
          <th>Invisible</th>
          <th>Invalid Location</th>
        </tr>
      </thead>
      <tbody>
        {Object.keys(testData).map((objectName: string, index: number) => {
          const object: SimObject = testData[objectName];
          return (
            <tr key={index}>
              <td>{objectName}</td>
              <td>{object.TOTAL}</td>
              <td>{object.Unknown}</td>
              <td>{object.Friendly}</td>
              <td>{object.Opposing}</td>
              <td>{object.Neutral}</td>
              <td>{object.Inactive}</td>
              <td>{object.Invisible}</td>
              <td>{object.InvalidLocation}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
);

export default GameTruthSummary;
