import GameTruthSummary from "@/components/GameTruthSummary";
import { GTIDATA } from "@/types";
import React, { SetStateAction, useEffect, useMemo, useState } from "react";
import { Menu, MenuItem, Sidebar, useProSidebar } from "react-pro-sidebar";
import DataGrid from "./DataGrid";

// SideBar Component. Displays on the side of the page and expands accross the top, over the map
const SideBarComponent = () => {
  const {
    entityRowData, // Row data for the entity state grid
    entityColumnDefs, // Column definitions for the entity state grid
    collapseSidebar, // Function to collapse or expand the sidebar
    collapsed, // Boolean flag indicating if the sidebar is collapsed or not
    setSideBarContent, // Function to set the content of the sidebar
  } = useSideBarState(); // Custom hook to manage state related to the sidebar

  const handleOpenTab = (contentName: SetStateAction<string>) => {
    setSideBarContent(contentName); // Set the content of the sidebar to the specified content name
    collapseSidebar(false); // Expand the sidebar
  };

  return (
    <Sidebar
      className="pro-sidebar"
      backgroundColor="rgb(41,43,44)"
      defaultCollapsed
      style={{
        position: "absolute",
        height: "80%",
      }}
    >
      <Menu className="pro-menu">
        <MenuItem
          icon={<i className="fa fa-table" />}
          onClick={() => handleOpenTab("entityState")}
          title="Entity State NG Packets"
        >
          {!collapsed && <span>Entity State NG Packets</span>}
        </MenuItem>
        <MenuItem
          icon={<i className="fa fa-circle-info" />}
          onClick={() => handleOpenTab("gameTruthSummary")}
          title="Game Truth Summary"
        >
          {!collapsed && <span>Game Truth Summary</span>}
        </MenuItem>
      </Menu>
      <div
        style={{
          position: "absolute",
          bottom: 0,
          width: "100%",
          padding: "10px",
        }}
      >
        <button
          className="btn btn-dark-outline text-light"
          onClick={() => collapseSidebar()}
        >
          <i className={`fa fa-chevron-${collapsed ? "right" : "left"}`} />
        </button>
      </div>
    </Sidebar>
  );
};

// custom hook used to handle all state for the SideBar component
const useSideBarState = () => {
  // State variables and hooks
  const [data, setData] = useState<GTIDATA[]>([]); // State variable 'data' of type 'GTIDATA[]' and its setter function 'setData' using useState hook
  const { collapseSidebar, collapsed } = useProSidebar(); // Custom hook 'useProSidebar' to get values for 'collapseSidebar' and 'collapsed'
  const [sidebarContent, setSideBarContent] = useState("none"); // State variable 'sidebarContent' initialized with default value "none" and its setter function 'setSideBarContent' using useState hook

  // Memoized entityColumnDefs
  const entityColumnDefs = useMemo(() => {
    const obj = data[0]?.GTIDATA?.EntityStateNGPacket?.[0]?.$; // Accessing nested object properties from 'data' using optional chaining
    if (obj) {
      const keys = Object.keys(obj); // Extracting keys from the object
      return keys.map((key) => ({
        // Creating an array of column definitions based on keys
        field: key, // Column field name
        value: obj[key], // Column value
        filter: true, // Column filter flag
        sortable: true, // Column sortable flag
        resizable: true, // Column resizable flag
      }));
    }
    return []; // Return an empty array if 'obj' is falsy
  }, [data]); // Memoize the column definitions based on changes in 'data'

  // Fetch XML data on mount
  useEffect(() => {
    if (data.length === 0) {
      // Condition to fetch data only if 'data' is empty
      const fetchXmlData = () => {
        fetch("/api/xmlData") // Fetch XML data from API endpoint
          .then((response) => response.json()) // Convert response to JSON
          .then((results) => {
            setData(results.data); // Update 'data' state with fetched data
          })
          .catch((error) => console.error(error)); // Handle error if any
      };
      fetchXmlData(); // Invoke the fetchXmlData function
    }
  }, [data]); // Run the effect only when 'data' changes

  // Compute entityRowData
  const entityRowData = data[0]?.GTIDATA?.EntityStateNGPacket?.map(
    (obj) => Object.values(obj)[0] // Extracting values from objects in the array
  );

  // Return an object with computed values
  return {
    entityRowData, // Computed 'entityRowData'
    entityColumnDefs, // Computed 'entityColumnDefs'
    collapseSidebar, // Value of 'collapseSidebar' from custom hook
    collapsed, // Value of 'collapsed' from custom hook
    sidebarContent, // Value of 'sidebarContent' state
    setSideBarContent, // Setter function for 'sidebarContent' state
  };
};

export default SideBarComponent;
