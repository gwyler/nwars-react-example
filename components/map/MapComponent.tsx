import React, { useEffect } from "react";
import * as Cesium from "cesium";
import { useDispatch } from "react-redux";
import { setViewer } from "./redux/mapReducer";

// Define the MapComponent
const MapComponent = () => {
  useMapState();
  return <div id="cesiumContainer" style={{ height: "100vh" }} />;
};

// Custom hook to handle the map state
const useMapState = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const viewer = new Cesium.Viewer("cesiumContainer", {
      terrainProvider: Cesium.createWorldTerrain(),
    });

    dispatch(setViewer(viewer));

    return () => {
      viewer.destroy();
    };
  }, [dispatch]);
};

// Export the MapComponent as the default export
export default MapComponent;
