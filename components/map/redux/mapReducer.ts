import { MapState } from "@/types";
import { createSlice } from "@reduxjs/toolkit";

const initialState: MapState = {
  homePosition: {
    west: -124.736342,
    south: 24.521208,
    east: -66.945392,
    north: 49.382808,
  },
  viewer: null,
};

export const mapSlice = createSlice({
  name: "map",
  initialState,
  reducers: {
    setHomePosition: (state, action) => {
      state.homePosition = action.payload;
    },
    setViewer: (state, action) => {
      state.viewer = action.payload;
    },
  },
});

export const { setHomePosition, setViewer } = mapSlice.actions;

export default mapSlice.reducer;
