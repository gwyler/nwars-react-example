import XYZ from "ol/source/XYZ";
import { GameData, TestData, TileLayerSources } from "./types";
import * as Cesium from "cesium";

export const testData: TestData = {
  ENTITIES: {
    TOTAL: 42532,
    Unknown: 0,
    Friendly: 0,
    Opposing: 29257,
    Neutral: 3275,
    Inactive: 1,
    Invisible: 2072,
    InvalidLocation: 0,
  },
  Land: {
    TOTAL: 34568,
    Unknown: 0,
    Friendly: 0,
    Opposing: 34568,
    Neutral: 0,
    Inactive: 0,
    Invisible: 1,
    InvalidLocation: 0,
  },
  Air: {
    TOTAL: 5506,
    Unknown: 0,
    Friendly: 0,
    Opposing: 4069,
    Neutral: 1437,
    Inactive: 0,
    Invisible: 2071,
    InvalidLocation: 0,
  },
  Surface: {
    TOTAL: 2111,
    Unknown: 0,
    Friendly: 0,
    Opposing: 273,
    Neutral: 1838,
    Inactive: 1,
    Invisible: 0,
    InvalidLocation: 0,
  },
  Subsurface: {
    TOTAL: 1,
    Unknown: 0,
    Friendly: 0,
    Opposing: 1,
    Neutral: 0,
    Inactive: 0,
    Invisible: 0,
    InvalidLocation: 0,
  },
  Space: {
    TOTAL: 0,
    Unknown: 0,
    Friendly: 0,
    Opposing: 0,
    Neutral: 0,
    Inactive: 0,
    Invisible: 0,
    InvalidLocation: 0,
  },
  Sensors: {
    TOTAL: 346,
    Unknown: 0,
    Friendly: 0,
    Opposing: 346,
    Neutral: 0,
    Inactive: 0,
    Invisible: 0,
    InvalidLocation: 0,
  },
  FACILITIES: {
    TOTAL: 58,
    Unknown: 0,
    Friendly: 0,
    Opposing: 58,
    Neutral: 0,
    Inactive: 0,
    Invisible: 1,
    InvalidLocation: 0,
  },
  EMITTERS: {
    TOTAL: 751,
    Unknown: 0,
    Friendly: 0,
    Opposing: 736,
    Neutral: 15,
    Inactive: 0,
    Invisible: 0,
    InvalidLocation: 0,
  },
  RADIOS: {
    TOTAL: 0,
    Unknown: 0,
    Friendly: 0,
    Opposing: 0,
    Neutral: 0,
    Inactive: 0,
    Invisible: 0,
    InvalidLocation: 0,
  },
  UNKOWN: {
    TOTAL: 0,
    Unknown: 0,
    Friendly: 0,
    Opposing: 0,
    Neutral: 0,
    Inactive: 0,
    Invisible: 0,
    InvalidLocation: 0,
  },
};

export const tileLayerSources: TileLayerSources = {
  dark: new XYZ({
    url: "https://{1-4}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png",
    attributions: "© CARTO",
  }),
  streets: new XYZ({
    url: "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png",
    attributions: "© OpenStreetMap Contributors",
  }),
  satellite: new XYZ({
    url: "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
    attributions: "© Esri",
  }),
  outdoors: new XYZ({
    url: "https://a.tile.opentopomap.org/{z}/{x}/{y}.png",
    attributions: "© OpenTopoMap Contributors",
  }),
  terrain: new XYZ({
    url: "https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.png",
    attributions: "© Stamen Design",
  }),
};

export const unifiedCombatantCommands = {
  pacom: {
    name: "USPACOM",
    bBox: {
      west: 80.0,
      south: -40.0,
      east: -150.0,
      north: 55.0,
    },
    imageURL: "/images/pacomAreaSquare.png",
  },
  centcom: {
    name: "USCENTCOM",
    bBox: {
      west: 25.0,
      south: 10.0,
      east: 80.0,
      north: 50.0,
    },
    imageURL: "/images/centcomAreaSquare.png",
  },
  africom: {
    name: "USAFRICOM",
    bBox: {
      west: -20.0,
      south: -35.0,
      east: 60.0,
      north: 40.0,
    },
    imageURL: "/images/africomAreaSquare.png",
  },
  eucom: {
    name: "USEUCOM",
    bBox: {
      west: -20.0,
      south: 35.0,
      east: 200.0,
      north: 100.0,
    },
    imageURL: "/images/eucomAreaSquare.png",
  },
  southcom: {
    name: "USSOUTHCOM",
    bBox: {
      west: -110.0,
      south: -60.0,
      east: -30.0,
      north: 15.0,
    },
    imageURL: "/images/southcomAreaSquare.png",
  },
  northcom: {
    name: "USNORTHCOM",
    bBox: {
      west: -170.0,
      south: 10.0,
      east: -50.0,
      north: 80.0,
    },
    imageURL: "/images/northcomAreaSquare.png",
  },
};

export const gameData: GameData[] = [
  {
    id: 1,
    name: "Example 1",
    factionName: "Blue - Taco 1",
    startDate: "2022-12-15",
    duration: 5,
    status: "Incomplete",
    phase: "Initial Setup",
    createdBy: "John",
    createdDate: "2022-10-05",
    updatedDate: "2022-12-15",
    unifiedCombatantCommand: unifiedCombatantCommands.pacom,
    collectionRequirements: 23,
  },
  {
    id: 2,
    name: "Example 2",
    factionName: "Green - Squad A",
    startDate: "2023-05-10",
    duration: 10,
    status: "Incomplete",
    phase: "Initial Setup",
    createdBy: "Alice",
    createdDate: "2023-04-15",
    updatedDate: "2023-04-27",
    unifiedCombatantCommand: unifiedCombatantCommands.eucom,
    collectionRequirements: 25,
  },
  {
    id: 3,
    name: "Example 3",
    factionName: "Blue - Platoon B",
    startDate: "2023-05-20",
    duration: 14,
    status: "Incomplete",
    phase: "Initial Setup",
    createdBy: "Bob",
    createdDate: "2023-04-10",
    updatedDate: "2023-04-27",
    unifiedCombatantCommand: unifiedCombatantCommands.africom,
    collectionRequirements: 8,
  },
  {
    id: 4,
    name: "Example 4",
    factionName: "Green - Task Force Delta",
    startDate: "2023-05-15",
    duration: 30,
    status: "Incomplete",
    phase: "Initial Setup",
    createdBy: "Samantha",
    createdDate: "2023-04-01",
    updatedDate: "2023-04-27",
    unifiedCombatantCommand: unifiedCombatantCommands.northcom,
    collectionRequirements: 18,
  },
  {
    id: 5,
    name: "Example 5",
    factionName: "Red - Company Z",
    startDate: "2023-06-01",
    duration: 21,
    status: "Ready to Run",
    phase: "Initial Setup",
    createdBy: "Charlie",
    createdDate: "2023-04-12",
    updatedDate: "2023-04-27",
    unifiedCombatantCommand: unifiedCombatantCommands.southcom,
    collectionRequirements: 5,
  },
  {
    id: 6,
    name: "Example 6",
    factionName: "Green - Squad C",
    startDate: "2023-05-01",
    duration: 14,
    status: "Running",
    phase: "Functional Testing",
    createdBy: "Emily",
    createdDate: "2023-04-01",
    updatedDate: "2023-04-27",
    unifiedCombatantCommand: unifiedCombatantCommands.centcom,
    collectionRequirements: 27,
  },
  {
    id: 7,
    name: "Example 7",
    factionName: "Red - Platoon D",
    startDate: "2023-06-01",
    duration: 7,
    status: "Paused",
    phase: "Operational Testing",
    createdBy: "Jack",
    createdDate: "2023-04-01",
    updatedDate: "2023-04-27",
    unifiedCombatantCommand: unifiedCombatantCommands.pacom,
    collectionRequirements: 15,
  },
  {
    id: 8,
    name: "Example 8",
    factionName: "Green - Bravo",
    startDate: "2022-02-01",
    duration: 10,
    status: "Running",
    phase: "Execution",
    createdBy: "Olivia",
    createdDate: "2022-01-19",
    updatedDate: "2022-11-27",
    unifiedCombatantCommand: unifiedCombatantCommands.africom,
    collectionRequirements: 20,
  },
  {
    id: 9,
    name: "Example 9",
    factionName: "Red - Alpha",
    startDate: "2023-06-01",
    duration: 3,
    status: "Incomplete",
    phase: "Initial Setup",
    createdBy: "Daniel",
    createdDate: "2022-03-08",
    updatedDate: "2022-10-11",
    unifiedCombatantCommand: unifiedCombatantCommands.centcom,
    collectionRequirements: 31,
  },
];

export const getTodaysDate = () => {
  const today = new Date();
  const year = today.getFullYear();
  const month = String(today.getMonth() + 1).padStart(2, "0");
  const day = String(today.getDate()).padStart(2, "0");

  return `${year}-${month}-${day}`;
};

export const createAndFlyToAOR = (game: GameData, viewer: Cesium.Viewer) => {
  const { west, south, east, north } = game.unifiedCombatantCommand.bBox;
  const layers = viewer.scene.imageryLayers;
  const baseLayer = viewer.imageryLayers.get(0);
  const workingArea = Cesium.ImageryLayer.fromProviderAsync(
    Cesium.IonImageryProvider.fromAssetId(2, {}),
    {
      rectangle: Cesium.Rectangle.fromDegrees(west, south, east, north),
    }
  );
  const boundingBox = Cesium.BoundingSphere.fromRectangle3D(
    Cesium.Rectangle.fromDegrees(west, south, east, north)
  );
  const center = Cesium.Cartographic.fromCartesian(boundingBox.center);
  // assuming the base layer is the first layer in the collection
  baseLayer.brightness = 0.1; // set brightness to 10% (darken the layer)

  while (layers.length > 1) {
    layers.remove(layers.get(1));
  }

  layers.add(workingArea);

  viewer.camera.flyTo({
    destination: Cesium.Cartesian3.fromRadians(
      center.longitude,
      center.latitude,
      10000000
    ),
  });
};
