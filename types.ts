import XYZ from "ol/source/XYZ";
import * as Cesium from "cesium";

export type DataGridProps = {
  columnDefs: any[];
  rowData: any[] | undefined;
  title: string;
};

interface EntityStateNGPacket {
  $: {
    [key: string]: any;
  };
}

export interface GTIDATA {
  GTIDATA?: {
    EntityStateNGPacket?: EntityStateNGPacket[];
  };
}

export interface ColumnDef {
  field: string;
  value: any;
}

export interface SimObject {
  TOTAL: number;
  Unknown: number;
  Friendly: number;
  Opposing: number;
  Neutral: number;
  Inactive: number;
  Invisible: number;
  InvalidLocation: number;
}

export interface TestData {
  [key: string]: SimObject;
  ENTITIES: SimObject;
  Land: SimObject;
  Air: SimObject;
  Surface: SimObject;
  Subsurface: SimObject;
  Space: SimObject;
  Sensors: SimObject;
  FACILITIES: SimObject;
  EMITTERS: SimObject;
  RADIOS: SimObject;
  UNKOWN: SimObject;
}

export interface TileLayerSources {
  [key: string]: XYZ;
}
export interface MapState {
  homePosition: Object;
  viewer: Cesium.Viewer | null;
}
export interface GameData {
  id: number;
  name: string;
  factionName: string;
  unifiedCombatantCommand: UnifiedCombatantCommands;
  startDate: string;
  duration: number;
  createdBy: string;
  createdDate: string;
  status: string;
  phase: string;
  updatedDate: string;
  collectionRequirements: number;
}

export interface RootState {
  [x: string]: any;
  map: MapState;
}
interface BBox {
  west: number;
  south: number;
  east: number;
  north: number;
}

export interface UnifiedCombatantCommands {
  name: string;
  bBox: BBox;
  imageURL: string;
}

export interface GameConfigState {
  [x: string]: any;
  gameName: string;
  selectedCommand: string;
}
