import { combineReducers } from "@reduxjs/toolkit";
import { configureStore } from "@reduxjs/toolkit";
import thunkMiddleware from "redux-thunk";
import mapReducer from "@/components/map/redux/mapReducer";
import gameConfigReducer from "@/components/GameConfig/redux/gameConfigReducer";

const rootReducer = combineReducers({
  map: mapReducer,
  gameConfig: gameConfigReducer,
});

const store = configureStore({
  reducer: rootReducer,
  middleware: [thunkMiddleware],
});

export default store;
