import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import store from "../redux/store";
import "bootstrap/dist/css/bootstrap.min.css";
import "@/styles/globals.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import { ProSidebarProvider } from "react-pro-sidebar";
import "cesium/Build/Cesium/Widgets/widgets.css";
import "ag-grid-community/styles//ag-grid.css";
import "ag-grid-community/styles//ag-theme-alpine.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ProSidebarProvider>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </ProSidebarProvider>
  );
}

export default MyApp;
