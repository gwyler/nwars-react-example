import fs from "fs";
import { parseString } from "xml2js";
import { NextApiRequest, NextApiResponse } from "next";
import path from "path";

const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  const folderPath = "C:\\Users\\garrett\\data\\"; // Specify the folder path here

  // Read all files in the folder
  const files = fs.readdirSync(folderPath);

  const dataArray: any[] = [];

  // Loop through each file
  for (const file of files) {
    // Read the XML file
    const xmlData = fs.readFileSync(path.join(folderPath, file), "utf-8");

    // Parse the XML data
    parseString(xmlData, (err, result) => {
      if (err) {
        console.error(err);
        return res
          .status(500)
          .json({ error: `Failed to parse XML data in file ${file}` });
      }
      // `result` is the parsed XML data in JavaScript object format
      dataArray.push(result);
    });
  }
  res.status(200).json({ data: dataArray });
};

export default handler;
